unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ExtCtrls,
  LCLIntf, StdCtrls, process, LazStringUtils;

type

  { TForm1 }

  TForm1 = class(TForm)
    ImageFundo: TImage;
    LabelMensagem: TLabel;
    LabelTempo: TLabel;
    MenuItemSobre: TMenuItem;
    MenuItemTitulo: TMenuItem;
    Separator2: TMenuItem;
    MenuItemSair: TMenuItem;
    MenuItemIniciarParar: TMenuItem;
    Separator1: TMenuItem;
    MenuItemConfiguracao: TMenuItem;
    PopupMenu1: TPopupMenu;
    Timer1: TTimer;
    TrayIcon1: TTrayIcon;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure MenuItemSairClick(Sender: TObject);
    procedure MenuItemConfiguracaoClick(Sender: TObject);
    procedure MenuItemIniciarPararClick(Sender: TObject);
    procedure MenuItemSobreClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public
    gInicio: Boolean;
    gPausado: Boolean;
    gConfigurando: Boolean;
    gLinhaExecutavelNavegador: String;
    gLinhaParametrosExecutavelNavegador: String;
    gImagemFundo: String;
    gStringListUrl: TStringList;
    gCorFundo: TColor;
    gLinhaUrl: Integer;
    gLinha: Integer;
    gTempo: Integer;
    gTempoContador: Integer;
    gProcessNavegador: TProcess;
  end;

var
  Form1: TForm1;

implementation

uses
  Unit2;

{$R *.lfm}

{ TForm1 }

function CaracterParaHtml(pTexto: String): String;
var
  fTextoHtml: String;
begin
  fTextoHtml := pTexto;

  while (Pos(' ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, ' ', '%20', [rfReplaceAll, rfIgnoreCase]);

  while (Pos('\', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, '\', '%5C', [rfReplaceAll, rfIgnoreCase]);

  while (Pos('À', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'À', '%C3%80', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Á', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Á', '%C3%81', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Â', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Â', '%C3%82', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ã', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ã', '%C3%83', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ä', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ä', '%C3%84', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Å', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Å', '%C3%85', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Æ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Æ', '%C3%86', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ç', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ç', '%C3%87', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('È', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'È', '%C3%88', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('É', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'É', '%C3%89', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ê', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ê', '%C3%8A', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ë', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ë', '%C3%8B', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ì', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ì', '%C3%8C', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Í', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Í', '%C3%8D', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Î', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Î', '%C3%8E', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ï', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ï', '%C3%8F', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ð', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ð', '%C3%90', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ñ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ñ', '%C3%91', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ò', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ò', '%C3%92', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ó', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ó', '%C3%93', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ô', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ô', '%C3%94', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Õ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Õ', '%C3%95', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ö', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ö', '%C3%96', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ø', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ø', '%C3%98', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ù', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ù', '%C3%99', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ú', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ú', '%C3%9A', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Û', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Û', '%C3%9B', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ü', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ü', '%C3%9C', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ý', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ý', '%C3%9D', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Þ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Þ', '%C3%9E', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ß', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ß', '%C3%9F', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('à', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'à', '%C3%A0', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('á', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'á', '%C3%A1', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('â', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'â', '%C3%A2', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ã', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ã', '%C3%A3', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ä', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ä', '%C3%A4', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('å', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'å', '%C3%A5', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('æ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'æ', '%C3%A6', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ç', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ç', '%C3%A7', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('è', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'è', '%C3%A8', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('é', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'é', '%C3%A9', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ê', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ê', '%C3%AA', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ë', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ë', '%C3%AB', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ì', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ì', '%C3%AC', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('í', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'í', '%C3%AD', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('î', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'î', '%C3%AE', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ï', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ï', '%C3%AF', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ð', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ð', '%C3%B0', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ñ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ñ', '%C3%B1', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ò', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ò', '%C3%B2', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ó', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ó', '%C3%B3', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ô', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ô', '%C3%B4', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('õ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'õ', '%C3%B5', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ö', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ö', '%C3%B6', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ø', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ø', '%C3%B8', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ù', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ù', '%C3%B9', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ú', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ú', '%C3%BA', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('û', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'û', '%C3%BB', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ü', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ü', '%C3%BC', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ý', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ý', '%C3%BD', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('þ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'þ', '%C3%BE', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ÿ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ÿ', '%C3%BF', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Œ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Œ', '%C5%92', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('œ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'œ', '%C5%93', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Š', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Š', '%C5%A0', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('š', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'š', '%C5%A1', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('Ÿ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'Ÿ', '%C5%B8', [rfReplaceAll, rfIgnoreCase]);
  while (Pos('ƒ', fTextoHtml) > 0) do fTextoHtml := StringReplace(fTextoHtml, 'ƒ', '%C6%92', [rfReplaceAll, rfIgnoreCase]);

  Result := fTextoHtml;
end;

function MyGetPart(pTag, pRegistro: String): String;
begin
  try
    Result := GetPart(['<' + LowerCase(pTag) + '>'], ['</' + LowerCase(pTag) + '>'], pRegistro, False, False);
  except
    Result := '';
  end;
end;

function iGetPart(pTag1, pTag2, pRegistro: String): String;
begin
  try
    Result := GetPart([pTag1], [pTag2], pRegistro, False, False);
  except
    Result := '';
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  fIndice: Integer;
  fUrl: String;
  fConteudoXml: String;
  fStringListXml: TStringlist;
begin
  gInicio := true;
  gPausado := false;
  gLinhaUrl := 0;
  gLinha := 0;
  gTempo := 30;
  gCorFundo := clBackground;
  gImagemFundo := '';
  gConfigurando := false;
  gStringListUrl := TStringList.Create;
  gProcessNavegador := TProcess.Create(nil);
  gLinhaExecutavelNavegador := 'C:\Program Files\Google\Chrome\Application\chrome.exe';
  gLinhaParametrosExecutavelNavegador := '[--incognito][--kiosk][<url>]';
  LabelTempo.Caption := '';
  LabelMensagem.Caption := '';

  if FileExists(ExtractFilePath(Application.ExeName) + 'slideurl_configuracao.xml') then begin
    fStringListXml := TStringlist.Create;

    try
      fStringListXml.LoadFromFile(ExtractFilePath(Application.ExeName) + 'slideurl_configuracao.xml');
    except
    end;

    fConteudoXml := fStringListXml.Text;

    if (Pos('<linhaexecutavelnavegador>', fConteudoXml) > 0) and (Pos('</linhaexecutavelnavegador>', fConteudoXml) > 0) then gLinhaExecutavelNavegador := MyGetPart('linhaexecutavelnavegador', fConteudoXml);
    if (Pos('<linhaparametrosexecutavelnavegador>', fConteudoXml) > 0) and (Pos('</linhaparametrosexecutavelnavegador>', fConteudoXml) > 0) then gLinhaParametrosExecutavelNavegador := MyGetPart('linhaparametrosexecutavelnavegador', fConteudoXml);
    if (Pos('<imagemfundo>', fConteudoXml) > 0) and (Pos('</imagemfundo>', fConteudoXml) > 0) then gImagemFundo := MyGetPart('imagemfundo', fConteudoXml);
    if (Pos('<corfundo>', fConteudoXml) > 0) and (Pos('</corfundo>', fConteudoXml) > 0) then gCorFundo := StrToIntDef(MyGetPart('corfundo', fConteudoXml), clBackground);
    if (Pos('<tempo>', fConteudoXml) > 0) and (Pos('</tempo>', fConteudoXml) > 0) then gTempo := StrToIntDef(MyGetPart('tempo', fConteudoXml), 30);

    if fStringListXml.Count > 0 then begin
      for fIndice := 0 to fStringListXml.Count - 1 do begin
        fConteudoXml := fStringListXml.Strings[fIndice];

        if (Pos('<url>', fConteudoXml) > 0) and (Pos('</url>', fConteudoXml) > 0) then begin
          fUrl := MyGetPart('url', fConteudoXml);

          if Length(Trim(fUrl)) > 0 then gStringListUrl.Add(fUrl);
        end;
      end;
    end;

    fStringListXml.Free;
  end;

  gProcessNavegador.Executable := gLinhaExecutavelNavegador;

  if FileExists(gImagemFundo) then begin
    try
      ImageFundo.Picture.LoadFromFile(gImagemFundo);

      ImageFundo.Visible := true;
    except
    end;
  end;

  Form1.BorderStyle := bsNone;
  Form1.FormStyle := fsNormal;
  Form1.WindowState := wsNormal;
  Form1.Color := gCorFundo;
  Form1.Left := 0;
  Form1.Top := 0;
  Form1.Width := Screen.Width;
  Form1.Height := Screen.Height;

  ImageFundo.Left := 0;
  ImageFundo.Top := 0;
  ImageFundo.Width := Form1.Width;
  ImageFundo.Height := Form1.Height;

  gTempoContador := 1;
  gInicio := false;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if gProcessNavegador.Running then begin
    try
      gProcessNavegador.Terminate(0);
    except
    end;
  end;

  gProcessNavegador.Free;
  gStringListUrl.Free;
end;

procedure TForm1.MenuItemSairClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.MenuItemConfiguracaoClick(Sender: TObject);
var
  fUrl: String;
  fIndice: Integer;
  StringListXml: TStringlist;
begin
  if gConfigurando then begin
    Form2.Show;
  end else begin
    gConfigurando := true;

    if Form2.ShowModal = mrOK then begin
      StringListXml := TStringlist.Create;
      gLinhaExecutavelNavegador := Form2.ComboBoxExecutavelNavegador.Text;
      gLinhaParametrosExecutavelNavegador := Form2.ComboBoxParametrosExecutavelNavegador.Text;
      gImagemFundo := Form2.EditImagemFundo.Text;
      gCorFundo := Form2.ColorButtonFundo.ButtonColor;
      gTempo := Form2.SpinEditTempo.Value;
      gTempoContador := 1;

      gStringListUrl.Assign(Form2.MemoUrl.Lines);

      with StringListXml do begin
        Add('<?xml version="1.0" encoding="UTF-8" ?>');
        Add('<dados>');
        Add('<linhaexecutavelnavegador>' + gLinhaExecutavelNavegador + '</linhaexecutavelnavegador>');
        Add('<linhaparametrosexecutavelnavegador>' + gLinhaParametrosExecutavelNavegador + '</linhaparametrosexecutavelnavegador>');
        Add('<imagemfundo>' + gImagemFundo + '</imagemfundo>');
        Add('<corfundo>' + IntToStr(gCorFundo) + '</corfundo>');
        Add('<tempo>' + gTempo.ToString + '</tempo>');
        Add('<urls>');

        if gStringListUrl.Count > 0 then begin
          for fIndice := 0 to gStringListUrl.Count - 1 do begin
            if Length(Trim(gStringListUrl.Strings[fIndice])) > 0 then Add('<url>' + gStringListUrl.Strings[fIndice] + '</url>');
          end;
        end;

        Add('</urls>');
        Add('</dados>');
        Add('<!-- F I M -->');
      end;

      if FileExists(gImagemFundo) then begin
        try
          ImageFundo.Picture.LoadFromFile(gImagemFundo);

          ImageFundo.Visible := true;
        except
        end;
      end;

      try
        StringListXml.SaveToFile(ExtractFilePath(Application.ExeName) + 'slideurl_configuracao.xml');
      except
        on E: Exception do begin
          LabelMensagem.Caption := 'Erro ao salvar configuração em ' + ExtractFilePath(Application.ExeName) + 'slideurl_configuracao.xml. ' + E.Message;
        end;
      end;

      StringListXml.Free;
    end;

    gConfigurando := false;

    if Form2.CheckBoxFecharPrograma.Checked then Close;
  end;
end;

procedure TForm1.MenuItemIniciarPararClick(Sender: TObject);
begin
  gPausado := not gPausado;
  MenuItemIniciarParar.Caption := BoolToStr(gPausado, 'Retomar', 'Parar');
end;

procedure TForm1.MenuItemSobreClick(Sender: TObject);
begin
  ShowMessage(
    'Por Ericson Benjamim.' + #13#10 + #13#10 +
    'Versão 1.0' + #13#10 + #13#10 +
    'ebarruda@trt13.jus.br'
  );
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  fParametro: String;
  fUrl: String;
  fLinhaParametrosExecutavelNavegador: String;
begin
  if not gConfigurando and not gInicio and not gPausado and (gStringListUrl.Count > 0) then begin
    Dec(gTempoContador);

    LabelTempo.Caption := gTempoContador.ToString;

    MenuItemTitulo.Caption := 'SLIDEURL (' + gTempoContador.ToString + ')';

    if gTempoContador = 0 then begin
      gTempoContador := gTempo;

      if gProcessNavegador.Running then begin
        try
          gProcessNavegador.Terminate(0);
        except
        end;
      end;

      gProcessNavegador.Parameters.Clear;

      fUrl := gStringListUrl.Strings[gLinhaUrl];

      if Length(Trim(fUrl)) > 0 then begin
        fLinhaParametrosExecutavelNavegador := gLinhaParametrosExecutavelNavegador;

        while (Pos('[', fLinhaParametrosExecutavelNavegador) > 0) and (Pos(']', fLinhaParametrosExecutavelNavegador) > 0) do begin
          fParametro := iGetPart('[', ']', fLinhaParametrosExecutavelNavegador);

          fLinhaParametrosExecutavelNavegador := StringReplace(fLinhaParametrosExecutavelNavegador, '[' + fParametro + ']', '', [rfReplaceAll]);

          gProcessNavegador.Parameters.Add(StringReplace(fParametro, '<url>', fUrl, [rfReplaceAll]));
        end;

        try
          gProcessNavegador.Execute;
        except
          on E: Exception do begin
            LabelMensagem.Caption := TimeToStr(Now) + ': ' +  E.Message;
          end;
        end;
      end;

      Inc(gLinhaUrl);

      if gLinhaUrl = gStringListUrl.Count then gLinhaUrl := 0;
    end;
  end else begin
    MenuItemTitulo.Caption := 'SLIDEURL (' + TimeToStr(Now) + ')';
  end;
end;

end.

