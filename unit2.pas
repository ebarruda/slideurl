unit Unit2;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Spin,
  ExtCtrls, Buttons, ExtDlgs;

type

  { TForm2 }

  TForm2 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ButtonExecutavelNavegadorSelecionar: TButton;
    ButtonImagemFundoSelecionar: TButton;
    CheckBoxFecharPrograma: TCheckBox;
    ColorButtonFundo: TColorButton;
    ComboBoxExecutavelNavegador: TComboBox;
    ComboBoxParametrosExecutavelNavegador: TComboBox;
    EditImagemFundo: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    MemoUrl: TMemo;
    OpenDialog1: TOpenDialog;
    OpenPictureDialog1: TOpenPictureDialog;
    SpinEditTempo: TSpinEdit;
    procedure ButtonExecutavelNavegadorSelecionarClick(Sender: TObject);
    procedure ButtonImagemFundoSelecionarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;

implementation

uses
  Unit1;

{$R *.lfm}

{ TForm2 }

procedure TForm2.FormCreate(Sender: TObject);
begin
  MemoUrl.Lines.Clear;

  ComboBoxExecutavelNavegador.Items.Clear;

  with ComboBoxExecutavelNavegador.Items do begin
    Add('C:\Program Files\Google\Chrome\Application\chrome.exe');
    Add('C:\Program Files\Mozilla Firefox\firefox.exe');
  end;

  ComboBoxParametrosExecutavelNavegador.Items.Clear;

  with ComboBoxParametrosExecutavelNavegador.Items do begin
    Add('[--kiosk][<url>]');
    Add('[-kiosk][<url>]');
  end;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  ComboBoxExecutavelNavegador.Text := Form1.gLinhaExecutavelNavegador;
  ComboBoxParametrosExecutavelNavegador.Text := Form1.gLinhaParametrosExecutavelNavegador;
  EditImagemFundo.Text := Form1.gImagemFundo;
  ColorButtonFundo.ButtonColor := Form1.gCorFundo;
  SpinEditTempo.Value := Form1.gTempo;

  Form2.MemoUrl.Lines.Assign(Form1.gStringListUrl);
end;

procedure TForm2.ButtonImagemFundoSelecionarClick(Sender: TObject);
begin
  if FileExists(EditImagemFundo.Text) then OpenPictureDialog1.InitialDir := ExtractFilePath(EditImagemFundo.Text);

  if OpenPictureDialog1.Execute then begin
    EditImagemFundo.Text := OpenPictureDialog1.FileName;
  end;
end;

procedure TForm2.ButtonExecutavelNavegadorSelecionarClick(Sender: TObject);
begin
  if FileExists(ComboBoxExecutavelNavegador.Text) then OpenDialog1.InitialDir := ExtractFilePath(ComboBoxExecutavelNavegador.Text);

  if OpenDialog1.Execute then begin
    ComboBoxExecutavelNavegador.Text := OpenDialog1.FileName;
  end;
end;

end.

